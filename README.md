# hdLearn - Home

Hi ! Here a big project start, the goal of hdLearn is to give open source and free ressources to do FPGA developpement.

## The hdLearn Project

This The project has only just begun, yet it already has major objectives, making open source resources accessible to as many people as possible for the development of FPGA systems on chips and why going as far as ASICs.  

This project also serves as a directory to follow advances in the world of HDL.

## Project Architecture

/!\ Every things is not here for now, but it will be. /!\

![](imgs/general_files_system_tree.png)

Each gray block is a subgroup and the white is repositories. If there is a "\*" at the beginning, it does not exist at the moment.

## Roadmap

I have to finish some projects before realy start this one.

## Contributing

I don't know how to accept contributors, soooo, send me a message and I will see then.

## Authors and acknowledgment

Just me for now ;) ... Who are "me" ? Oh, yes, my name Thibault, and you can send me message to hdlearn.contact@protonmail.com (I think I will check this mail at least once a month).

## License

MIT license for this project but keep warning, all of the contents you can find here it's not necessarily on the same license.
